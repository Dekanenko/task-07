-- MySQL Workbench Forward Engineering
DROP database IF EXISTS `testdb`;

CREATE database `testdb`;

USE `testdb`;


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `testdb` DEFAULT CHARACTER SET utf8 ;
USE `testdb` ;

-- -----------------------------------------------------
-- Table `db`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `testdb`.`users` ;

CREATE TABLE IF NOT EXISTS `testdb`.`users` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `login` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY(`login`))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db`.`teams`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `testdb`.`teams` ;

CREATE TABLE IF NOT EXISTS `testdb`.`teams` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY(`name`))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db`.`users_teams`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `testdb`.`users_teams` ;

CREATE TABLE IF NOT EXISTS `testdb`.`users_teams` (
    `user_id` INT NULL,
    `team_id` INT NULL,
    INDEX `fk_users_has_teams_users_idx` (`user_id` ASC) VISIBLE,
    INDEX `fk_users_has_teams_teams1_idx` (`team_id` ASC) VISIBLE,
    CONSTRAINT `fk_users_has_teams_users`
    FOREIGN KEY (`user_id`)
        REFERENCES `testdb`.`users` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT `fk_users_has_teams_teams1`
    FOREIGN KEY (`team_id`)
        REFERENCES `testdb`.`teams` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE)
    ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

SELECT * FROM users;
SELECT * FROM teams;
