package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

    private static final String FULL_URL = "jdbc:derby:memory:testdb;create=true";
    private static DBManager instance;
    public static synchronized DBManager getInstance() {
        if(instance == null)
            instance = new DBManager();
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> list = new ArrayList<>();
        try(Connection con = DriverManager.getConnection(FULL_URL);
            Statement stmp = con.createStatement();
            ResultSet r = stmp.executeQuery("SELECT * FROM users ORDER BY id"))
        {
            while (r.next()){
                User user = new User();
                user.setId(r.getInt("id"));
                user.setLogin(r.getString("login"));
                list.add(user);
            }
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new DBException("Find exception", ex);
        }
        return list;
    }

    public boolean insertUser(User user) throws DBException {
        try(Connection con = DriverManager.getConnection(FULL_URL);
            PreparedStatement stmp = con.prepareStatement("INSERT INTO users (id, login) VALUES (DEFAULT, ?)"))
        {
            stmp.setString(1,user.getLogin());
            return stmp.execute();
        }catch (SQLException ex){
            ex.printStackTrace();
            return false;
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        try(Connection con = DriverManager.getConnection(FULL_URL);
            PreparedStatement stmt = con.prepareStatement("DELETE FROM users WHERE login=?")){
            for(User user : users){
                stmt.setString(1, user.getLogin());
                if(stmt.execute())
                    return false;
            }
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new DBException("Delete User Exception", ex);
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = null;
        try(Connection con = DriverManager.getConnection(FULL_URL);
            PreparedStatement stmp = con.prepareStatement("SELECT * FROM users WHERE login=?"))
        {
            stmp.setString(1, login);
            ResultSet rs = stmp.executeQuery();
            rs.next();
            user = new User();
            user.setId(rs.getInt("id"));
            user.setLogin(rs.getString("login"));
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new DBException("GetUser", ex);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        try(Connection con = DriverManager.getConnection(FULL_URL);
            PreparedStatement stmp = con.prepareStatement("SELECT * FROM teams WHERE name=?"))
        {
            stmp.setString(1, name);
            ResultSet rs = stmp.executeQuery();
            rs.next();
            team = new Team();
            team.setId(rs.getInt("id"));
            team.setName(rs.getString("name"));
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new DBException("GetUser", ex);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> list = new ArrayList<>();
        try(Connection con = DriverManager.getConnection(FULL_URL);
            Statement stmp = con.createStatement();
            ResultSet r = stmp.executeQuery("SELECT * FROM teams ORDER BY id");)
        {
            while (r.next()){
                Team team = new Team();
                team.setId(r.getInt("id"));
                team.setName(r.getString("name"));
                list.add(team);
            }
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new DBException("FIND", ex);
        }
        return list;
    }

    public boolean insertTeam(Team team) throws DBException {
        try(Connection con = DriverManager.getConnection(FULL_URL);
            PreparedStatement stmp = con.prepareStatement("INSERT INTO teams (id, name) VALUES (DEFAULT, ?)"))
        {
            stmp.setString(1, team.getName());
            return stmp.execute();
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new DBException("Insert", ex);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        PreparedStatement stmtUser = null;
        PreparedStatement stmtTeam = null;
        ResultSet teamRs = null;

        int check = 0;
        try{
            HashSet<Team> set = (HashSet<Team>) getUserTeams(user).stream().collect(Collectors.toSet());

            con = DriverManager.getConnection(FULL_URL);
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            stmt = con.prepareStatement("INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)");
            stmtUser = con.prepareStatement("SELECT * FROM users WHERE login=?");
            stmtTeam = con.prepareStatement("SELECT * FROM teams WHERE name=?");
            stmtUser.setString(1, user.getLogin());
            ResultSet userRs = stmtUser.executeQuery();
            userRs.next();

            for(Team team : teams){
                if(set.contains(team)){
                    throw new SQLException();
                }

                stmtTeam.setString(1, team.getName());
                teamRs = stmtTeam.executeQuery();
                teamRs.next();
                stmt.setInt(1, userRs.getInt("id"));
                stmt.setInt(2, teamRs.getInt("id"));
                check = stmt.executeUpdate();
                if(check!=1){
                    System.out.println("s");
                    return false;
                }
            }
            con.commit();
        }catch (SQLException ex){
            ex.printStackTrace();
            try{
                con.rollback();
            }catch (SQLException e){
                e.printStackTrace();
                throw new DBException("Set Teams For User Exception", e);
            }
            throw new DBException("Set Teams For User Exception", ex);
        }finally {
            try {
                con.close();
                stmt.close();
            }catch (SQLException ex){
                ex.printStackTrace();
                throw new DBException("Close", ex);
            }
        }
        return false;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        PreparedStatement stmtUser = null;
        List<Team> list = new ArrayList<>();
        try(Connection con = DriverManager.getConnection(FULL_URL);
            PreparedStatement stmp = con.prepareStatement("SELECT team_id FROM users_teams WHERE user_id=?"))
        {
            stmtUser = con.prepareStatement("SELECT * FROM users WHERE login=?");
            stmtUser.setString(1, user.getLogin());
            ResultSet userRs = stmtUser.executeQuery();
            userRs.next();
            stmp.setInt(1, userRs.getInt("id"));
            ResultSet rs = stmp.executeQuery();
            while(rs.next()){
                PreparedStatement stmtIn = con.prepareStatement("SELECT * FROM teams WHERE id=?");
                stmtIn.setInt(1, rs.getInt("team_id"));
                ResultSet rsIN = stmtIn.executeQuery();
                rsIN.next();
                Team team = new Team();
                team.setId(rsIN.getInt("id"));
                team.setName(rsIN.getString("name"));
                list.add(team);
            }
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new DBException("GetUser", ex);
        }
        return list;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try(Connection con = DriverManager.getConnection(FULL_URL);
            PreparedStatement stmt = con.prepareStatement("DELETE FROM teams WHERE name=?"))
        {
            stmt.setString(1, team.getName());
            return stmt.execute();
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new DBException("Delete Team Exception", ex);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        PreparedStatement teamStatement = null;
        try(Connection con = DriverManager.getConnection(FULL_URL);
            PreparedStatement stmt = con.prepareStatement("UPDATE teams SET name=? WHERE name=?"))
        {
            stmt.setString(1, team.getName());
            stmt.setString(2, team.getOldName());
            return stmt.execute();
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new DBException("Update Team Exception", ex);
        }
    }

}

